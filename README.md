# Curso Javascript
## Clase 1 (npm)
### ¿Qué es NPM?
**NPM es el administrador de módulos para Node.js.** Fue creado el 2009 como un proyecto open source para ayudar a los desarrolladores Javascript a compartir fácilmente sus módulos en Node.js.
El registro de NPM es una colección de módulos open source para Node.js, front-end web apps, mobile apps, robots, routers, y un sinnúmero de otras necesidades de la comunidad JavaScript.
NPM cuenta con un cliente de línea de comandos que permite a los desarrolladores instalar y publicar estos módulos.
**Npm, Inc.** es la compañía que aloja y mantiene todo lo registrado en NPM.

### ¿Qué es un módulo o paquete en Node.js?
Un módulo en Node.js es simplemente una unidad de código relacionado con un único fin.
Un módulo puede estar compuesto por 1 o más archivos, Node.js tiene un sistema simple de carga de módulos, En Node.js, los archivos y los módulos tienen una correspondencia de uno a uno, Un módulo puede depender de uno o más módulos de Node.js.

Existen 2 tipos de módulos: **los módulos principales(Core modules)** y **los módulos de archivo(File modules).**

#### **Módulos principales**
Los módulos principales(nativos) son definidos en el código fuente de node en la carpeta lib/ y tienen la preferencia de carga frente a cualquier otro módulo.

#### **Módulos de archivo**
Los módulos de archivo son módulos definidos por el usuario(como el que usaremos en este artículo) y por lo general se registran en NPM para su uso.
Conceptos básicos de un módulo de archivo en Node.js
Primero, definiremos una estructura de trabajo para nuestro módulo npm(lo llamaremos: curso-javascript), existe un estándar para organizarlo, pero yo prefiero modificar un poco la estructura para acomodarla más a mis necesidades.

**Estructura Estandar**
```
    curso-javascript/
    ├── build
    |   └─ build.js
    ├── node_modules
    ├── source/
    |   └─ index.js
    ├── test/
    |   └─ index.js 
    ├── .gitignore
    ├── gulpfile.js
    ├── package.json
    └── README.md
```
**Estructura modificada base**
```
    curso-javascript/
    ├── dist
    |   └─ app.js (local)
    |   └─ app.min.js (production)
    ├── node_modules
    ├── src/
    |   └─ index.js
    ├── test/
    |   └─ index.js 
    ├── .gitignore
    ├── gulpfile.js
    ├── package.json
    └── README.md
```

**Donde:**

**La carpeta  src/:**
Es la que contendrá nuestro código fuente(en nuestro caso aquí irá todo nuestro código fuente en javascript).

**La carpeta dist/:** Es la que contendrá nuestro código Javascript final(el resultado de la compilación del código fuente en javascript).

**La carpeta test/:** Es la que contendrá el código Javascript final de las pruebas unitarias que tenga nuestro módulo(no son obligatorias, pero es una buena práctica).

**La carpeta node_modules/:** Es una carpeta donde esta los plugins descargados desde NPM y del core de node.js
El archivo .npmignore: Este archivo es necesario para que NPM sepa que archivos de nuestro repositorio ignorar al instalar y descargar nuestro módulo en una máquina(ordenador) cliente.

**El archivo package.json:** Este archivo es necesario para que NPM sepa toda la información relevante de nuestro proyecto.
Por ejemplo: nombre, versión, script principal, scripts secundarios,  autor, tipo de licencia, dependencias, dependencias de desarrollo y mucho más.

**El archivo README.md:** Este es un archivo con extensión .md( Markdown), por defecto es el archivo llamado por NPM para mostrar un resumen y/o instrucciones de instalación de nuestro módulo o paquete Node.js.

En esta parte es donde desarrollamos nuestro módulo, codeamos y realizamos la funcionalidad y probamos que todo vaya bien(espero realizar un artículo para profundizar la creación de un módulo en Node.js).
### **Cómo crear archivo package.json**
- name: curso-javascript (nombre del proyecto)
- version: 0.1.0 (Version del proyecto)
- description: Curso sobre javascript (descripcion del proyecto)
- entry point: index.js (punto de entrada a la aplicación) 
- test command: (comandos para nuestra aplicación)
- git repository: git@gitlab.com:viga.23/curso-javascript.git (url del repositorio git)
- keywords: curso, sobre, npm, javascript, gulp (palabras claves del proyecto)
 - autor: Alver Grisales <viga.23@hotmail.com> (Autor del proyecto)

## Clase 2 GULP
#### Paso 1
- Instalamos gulp globalmente en nuestra maquina, con el siguiente comando:

    ```sh
    npm install --global gulp
    ```
- Instalamos gulp en el proyecto
    ```sh
    npm install gulp --save-dev
    ```
- Creamos el archivo gulpfile.js
- Creamos la estructura del proyecto, para eso usamos npm y creamos el siguienté comando en nuestro archivo package.json:

    ```sh
    "scripts": {
        "src": "mkdir -p src/ src/img src/js src/stylus",
        "build": "mkdir -p dist/ dist/img dist/js dist/css"
  }
    ```
#### Paso 2
##### La API de Gulp es increíblemente ligera que contiene 4 funciones de nivel superior, son :
 *  **gulp.task:** Con gulp task definimos nuestra taQrea a crear, sus argumentos, dependencias y funciones.

    ```javascript
    gulp.task('mytask', function() {
      //mi tarea a ejecutar
    });
    gulp.task('dependenttask', ['mytask'], function() {
      //mi tarea despues de ejecutar 'mytask'
    });
    ```
 * **gulp.src:** la ruta de ls archivos a utilizar, su utiliza pipe para cambiar el lugar de salida del archivo
 
    ```javascript
    gulp.task('copyHtml', function() {
      // Copiamos los archivo HTML de src y los enviamos a dist
      gulp.src('src/*.html').pipe(gulp.dest('dist'));
    });
    ```
 *  **gulp.dest:** gulp dest apunta a la carpeta de salida que queremos escribir archivos.
 * **gulp.watch:** gulp watch nos sirve para crear tareas de monitoreo.
 
    ```javascript
    gulp.watch('src/js/**/*.js', ['myTask']);
    ```
#### Paso 3
- Instalamos standard con el siguiente comando:

     ```sh
    npm install --save-dev gulp-standard
     ```
#### Ejemplo de Uso

    ```javascript
    
    /* File: gulpfile.js */
    // grab our packages
    var gulp   = require('gulp')
    var standard = require('gulp-standard')
    
    // define the default task and add the watch task to it
    gulp.task('default', ['watch']);
    
    // configure the jshint task
    gulp.task('standard', function() {
      return gulp.src('src/js/**/*.js')
        .pipe(standard())
        .pipe(standard.reporter('default', {
            breakOnError: true
        }))
    });
    ```
#### Paso 4
- Instalamos los plugins faltantantes para crear nuestras tareas de js y css:

    ```sh
    npm install gulp-sourcemaps gulp-uglify gulp-util gulp-concat gulp-minify-css
    ```
#### Ejemplo de Uso completo

    ```javascript

    var gulp = require('gulp')
    var standard = require('gulp-standard')
    var sourcemaps = require('gulp-sourcemaps')
    var uglify = require('gulp-uglify')
    var gulpUtil = require('gulp-util')
    var concat = require('gulp-concat')
    var minifyCss = require('gulp-minify-css')
    
    var config = {
    	script: {
    		src: 'src/js/**/*.js',
    		dest: 'dist/js'
    	},
    	css: {
    		src: 'src/css/**/*.css',
    		dest: 'dist/css'
    	}
    }
    
    gulp.task('standard', function () {
    	return gulp.src(config.script.src)
    	.pipe(standard())
    	.pipe(standard.reporter('default', {
    		breakOnError: true
    	}))
    })
    
    gulp.task('build:js', function () {
    	return gulp.src(config.script.src)
    	.pipe(sourcemaps.init())
    	.pipe(concat('bundle.js'))
    	.pipe(gulpUtil.env.type === 'production' ? uglify() : gulpUtil.noop())
    	.pipe(sourcemaps.write())
    	.pipe(gulp.dest(config.script.dest))
    })
    
    gulp.task('build:css', function () {
    	return gulp.src(config.css.src)
    	.pipe(concat('bundle.css'))
    	.pipe(gulpUtil.env.type === 'production' ? minifyCss() : gulpUtil.noop())
    	.pipe(gulp.dest(config.css.dest))
    })
    
    gulp.task('bundle', ['standard', 'build:js', 'build:css'])
    
    gulp.task('default', ['bundle'])
    ```

